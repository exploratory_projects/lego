# LEGO

Все любят конструктор Lego, если только, конечно, вы когда-нибудь на него не наступали :) 

В этом проекте я собираюсь проанализировать набор данных о каждом блоке Lego, который когда-либо был построен!

Everyone loves Lego, unless you ever stepped on one. 

In this project, we will analyze a fascinating dataset on every single Lego block that has ever been built!

